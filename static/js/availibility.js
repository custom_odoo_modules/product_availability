odoo.define('product_availability.availibility', function (require) {
    var PublicWidget = require('web.public.widget');
    var Availibility = PublicWidget.Widget.extend({
        selector: 'section.availibility',
        start: function () {
            const section = this.el;
            const variants_grid = section.querySelector('.variants_grid')
            const stock_grid = section.querySelector('.stock_grid')
            let variants = variants_grid.querySelectorAll('[product-id]')
            let start_value = getHashValue('attr') ?? variants[0]?.getAttribute('product-id') ?? ''
            setVariant(start_value)
            console.log(start_value)
            variants.forEach(variant => {
                variant.addEventListener('click', () => {
                    let variant_id = variant.getAttribute('product-id')
                    setVariant(variant_id)
                })
            });

            function getHashValue(key) {
                var matches = location.hash.match(new RegExp(key+'=([^&]*)'));
                return matches ? matches[1] : null;
            }
            
            function setVariant(value){
                let stocks = stock_grid.querySelectorAll('[product-id]')
                let no_products = stock_grid.querySelector('.no_product')
                if(stock_grid.querySelector(`[product-id="${value}"]`)){
                    stocks.forEach(stock => {
                        let stock_id = stock.getAttribute('product-id')
                        if(stock_id === value){
                            stock.classList.remove('d-none')
                        }
                        else{
                            stock.classList.add('d-none')
                        }
                    })
                    no_products.classList.add('d-none')
                }
                else{
                    stocks.forEach(stock => {
                        stock.classList.add('d-none')
                    })
                    no_products.classList.remove('d-none')
                }
                variants.forEach(variant => {
                    let variant_id = variant.getAttribute('product-id')
                    if(variant_id === value){
                        variant.classList.add('active')
                    }
                    else{
                        variant.classList.remove('active')
                    }
                })
            }
        },
    });
    PublicWidget.registry.availibility = Availibility;
    return Availibility;
 });
