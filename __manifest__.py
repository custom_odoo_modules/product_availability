# -*- coding: utf-8 -*-
{
    'name': "Наличие продуктов",

    'summary': """
        Наличие продуктов""",

    'author': "Xorikita",
    'website': "https://erpsmart.ru",
    'sequence': -2,
    'category': 'Inventory/Inventory',
    'version': '15.0.1.0.0',

    'depends': ['website_sale', 'stock',],

    'images': [
      'static/description/icon.png',
    ],

    'data': [
        'views/views.xml',
        'views/availability.xml',
    ],
    'assets': {
        'web.assets_frontend': [
            ('prepend', 'product_availability/static/scss/main.scss'),
            '/product_availability/static/js/availibility.js',
        ],
    },
    'licence': 'LGPL-3'
}
